import Vue from "vue";
import Router from "vue-router";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";
import App from './App.vue'

Vue.use(Router);

const router = new Router({
    // ...
});

Sentry.init({
    Vue,
    dsn: "https://f16050ab5cd24b2ba53013c60fb49dbb@o1085167.ingest.sentry.io/6095647",
    integrations: [
        new Integrations.BrowserTracing({
            routingInstrumentation: Sentry.vueRouterInstrumentation(router),
            tracingOrigins: ["localhost", "my-site-url.com", /^\//],
        }),
    ],
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
});

// ...

new Vue({
    router,
    render: h => h(App),
}).$mount("#app");